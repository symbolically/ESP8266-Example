#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266NetBIOS.h>

ESP8266WebServer server;

void handle_root()
{
    server.send(200, "text/html", "<html><body><p>Hello World</p></body></html>");
}

void handle_on()
{
    digitalWrite(LED_BUILTIN, LOW);
    server.send(200, "text/html", "<html><body><p>LED On</p></body></html>");
}

void handle_off()
{
    digitalWrite(LED_BUILTIN, HIGH);
    server.send(200, "text/html", "<html><body><p>LED Off</p></body></html>");
}

void setup(void) {
    Serial.begin(115200);
    WiFi.mode(WIFI_STA);
    WiFi.begin("LIUYU", "12345678");
    while (!WiFi.isConnected())
    {
        delay(500);
        Serial.print('.');
    }
    Serial.println(WiFi.localIP());

    server.on("/", handle_root);
    server.on("/on", handle_on);
    server.on("/off", handle_off);
    server.begin();

    NBNS.begin("light");

    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, HIGH);
}

void loop(void) {
    server.handleClient();
}
