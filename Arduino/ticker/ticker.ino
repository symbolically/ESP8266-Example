#include <Ticker.h>

Ticker timer;

void toggle()
{
    digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
}

void setup() {
    Serial.begin(115200);
    pinMode(LED_BUILTIN, OUTPUT);
    timer.attach(1, toggle);
}

void loop() {
    if (Serial.available())
    {
        Serial.print(Serial.readString());
    }
}
