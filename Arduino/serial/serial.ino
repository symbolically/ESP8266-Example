void setup() {
    pinMode(LED_BUILTIN, OUTPUT);
    Serial.begin(115200);
    Serial.println("");
}

void loop() {
    if (Serial.available()) {
        String s = Serial.readString();
        if (s == "on") {
            digitalWrite(LED_BUILTIN, LOW);
        }
        if (s == "off") {
            digitalWrite(LED_BUILTIN, HIGH);
        }
    }
}
