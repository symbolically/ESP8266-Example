#include <Adafruit_NeoPixel.h>

Adafruit_NeoPixel pixels(8, D4, NEO_GRB + NEO_KHZ800);

void setup() {
    pixels.begin();
}

void loop() {
    int r, g, b;
    r = g = b = 255;
    for (int i = 0; i < 8; i++)
    {
        if (i % 3 == 0) {
            r = 255 ;
            g = 0;
            b = 0;
        }
        if (i % 3 == 1) {
            r = 0 ;
            g = 255;
            b = 0;
        }
        if (i % 3 == 2) {
            r = 0 ;
            g = 0;
            b = 255;
        }
        pixels.setPixelColor(i, r, g, b);
    }
    pixels.show();
    delay(1000);
    for (int i = 0; i < 8; i++)
    {
        if (i % 3 == 0) {
            r = 0 ;
            g = 255;
            b = 0;
        }
        if (i % 3 == 1) {
            r = 0 ;
            g = 0;
            b = 255;
        }
        if (i % 3 == 2) {
            r = 255 ;
            g = 0;
            b = 0;
        }
        pixels.setPixelColor(i, r, g, b);
    }
    pixels.show();
    delay(1000);
    for (int i = 0; i < 8; i++)
    {
        if (i % 3 == 0) {
            r = 0 ;
            g = 0;
            b = 255;
        }
        if (i % 3 == 1) {
            r = 255 ;
            g = 0;
            b = 0;
        }
        if (i % 3 == 2) {
            r = 0 ;
            g = 255;
            b = 0;
        }
        pixels.setPixelColor(i, r, g, b);
    }
    pixels.show();
    delay(1000);
}
