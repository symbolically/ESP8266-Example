#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>

void setup() {
    Serial.begin(115200);
    Serial.println();
    WiFi.mode(WIFI_STA);
    WiFi.begin("众创空间", "zckj11081109");
    while(!WiFi.isConnected())
    {
        delay(100);
    }
    Serial.println(WiFi.localIP());

    WiFiClient client;
    HTTPClient http;
    http.begin(client, "http://www.nmc.cn/f/rest/real/57143");
    if (http.GET() == 200)
    {
        //Serial.print(http.getString());
        const size_t capacity = JSON_OBJECT_SIZE(3) + JSON_OBJECT_SIZE(5) + JSON_OBJECT_SIZE(7) + JSON_OBJECT_SIZE(8) + JSON_OBJECT_SIZE(10) + 400;
        DynamicJsonDocument doc(capacity);
        deserializeJson(doc, http.getString());
        const char* info = doc["weather"]["info"];
        float temp = doc["weather"]["temperature"];
        Serial.print("天气：");
        Serial.println(info);
        Serial.print("温度：");
        Serial.println(temp);
    }
    http.begin(client, "http://www.nmc.cn/f/rest/tempchart/57143");
    if (http.GET() == 200)
    {
        const size_t capacity = JSON_ARRAY_SIZE(14) + 14*JSON_OBJECT_SIZE(5) + 890;
        DynamicJsonDocument doc(capacity);
        deserializeJson(doc, http.getString());
        for (int i = 0; i < doc.size(); i++) {
            const char* realTime = doc[i]["realTime"];
            Serial.print(realTime);
            Serial.print(",");
            float maxTemp = doc[i]["maxTemp"];
            Serial.print(maxTemp);
            Serial.print(",");
            float minTemp = doc[i]["minTemp"];
            Serial.print(minTemp);
            Serial.println();
        }
    }
    http.end();
}

void loop() {}
